import shutil, os
import csv

#------------CONFIG-----------------------
INPUT_FOLDER = 'FS_SAMPLES/'
OUTPUT_FOLDER = '_SMEARED/'
SAMPLENAME = 'gyroporuscastaneus'
LATINS_FILE = 'latins.csv'
#------------EOF CONFIG-------------------

def renameAndIncludeImage(tagged, bridge, input_image_file, counter):
    if tagged:
        id = shroom_url_tag(input_image_file.name,False)
        tag_status = '_T'
    else:
        id = shroom_url_bestguess(input_image_file.name,False,False)
        tag_status = ''
    #assembly name
    new_filename = str(counter) + '_' + str(id) + tag_status +'.jpg'
    mushroom = bridge[bridge['id']==id]['name'].iloc[0]

    if not os.path.exists(OUTPUT_FOLDER):
        os.makedirs(OUTPUT_FOLDER )
    shutil.copyfile(input_image_file, OUTPUT_FOLDER  + new_filename)

def handleRecord():
    for x in range(1, 5):
        oldfile = 'zzz_full_{}_{}.webp'.format(SAMPLENAME, x)
        newfile = 'zzz_full_{}_{}.webp'.format(row["latinId"], x)
        shutil.copyfile(INPUT_FOLDER + oldfile, OUTPUT_FOLDER + newfile)

def truncateOutput():
    if os.path.exists(OUTPUT_FOLDER):
        shutil.rmtree(OUTPUT_FOLDER)
    os.mkdir(OUTPUT_FOLDER)


#Clear output
truncateOutput()

# MAIN
truncateOutput()

with open(LATINS_FILE, newline='') as f:
    reader = csv.DictReader(f, fieldnames=["latinId"])
    latinIds = [row for row in reader]

    for row in latinIds:
        handleRecord()







