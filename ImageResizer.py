from PIL import Image

import shutil, os
import csv

#------------CONFIG-----------------------
INPUT_FOLDER = 'FS_REPO/'
OUTPUT_FOLDER = '_RESIZED/'
NEWSIZE = (1280, 720)
CROP = False
NORMALIZE_NAME = True
LATINID_RESOLVER = {
    "ammanitacitrina": "ammanitamappa",
    "chalciporuspiperatus": "suilluspiperatus",
    "cortinariuscaperatus": "rozitescaperatus",
    "clitocybeconnata": "lyophyllumconnatum",
    "discinaperlata": "discinaancilis",
    "entolomasinuatum": "entolomaeulividum",
    "ganodermaapplanatum": "ganodermalipsiense",
    "gymnopusdryophilus": "collybiadryophila",
    "hericiumcoralloides": "hericiumclathroides",
    "lactariusturpis": "lactariusnecator",
    "leccinumversipelle" : "leccinumrufescens",
    "macrolepiotarachodes": "macrolepiotarhacodes",
    "mycetinisalliaceus": "marasmiusalliaceus",
    "pezizabadia": "pezzizabadia",
    "phylloporuspelletieri": "phylloporusrhodoxanthus",
    "rhodocollybiaasema": "collybiaasema"

}
#------------EOF CONFIG-------------------

for entry in os.scandir(INPUT_FOLDER):
    print(entry)
    im = Image.open(INPUT_FOLDER+'/'+entry.name)

    imr = im.resize(NEWSIZE)

    # utility to deal with latinIDs conflicts
    if NORMALIZE_NAME:
        basename = entry.name.lower()

        for k, v in LATINID_RESOLVER.items() :
            basename = basename.lower().replace(k, v)

    else:
        basename = entry.name.lower()

    if CROP:
        crop_rectangle = (100, 0, 600, 400)
        imc = imr.crop(crop_rectangle)
        imc.save(OUTPUT_FOLDER + '/' + basename.replace("jpg", "webp").lower(), format='WEBP', subsampling=0, quality=80)
    else:
        imr.save(OUTPUT_FOLDER + '/' + basename.replace("jpg", "webp")
                 .replace("full_", "")
                 .lower(), format='WEBP', subsampling=0, quality=75)
