import pandas as pd
import shutil, os

#------------CONFIG-----------------------
INPUT_FOLDER_TAG = 'TAG_REPO/'
INPUT_FOLDER_NOTAG = 'NOTAG_REPO/'
OUTPUT_FOLDER = '_OUTPUT/'
OUTPUT_SUBFOLDER_IMAGES = 'IMG/'
BRIDGE_FILE = 'idToNameBridge.csv'
HTML_TEMPLATE = 'template.html'
#------------EOF CONFIG-------------------

def readBridge():
    data = pd.read_csv(BRIDGE_FILE)
    return data

def createHtmlSkeleton(index=1):
    output_file = OUTPUT_FOLDER + 'sheet' + str(index) + '.html'
    shutil.copyfile(HTML_TEMPLATE, output_file)
    return output_file

def renameAndIncludeImage(tagged, bridge, input_image_file, counter):
    if tagged:
        id = shroom_url_tag(input_image_file.name,False)
        tag_status = '_T'
    else:
        id = shroom_url_bestguess(input_image_file.name,False,False)
        tag_status = ''
    #assembly name
    new_filename = str(counter) + '_' + str(id) + tag_status +'.jpg'
    mushroom = bridge[bridge['id']==id]['name'].iloc[0]

    if not os.path.exists(OUTPUT_FOLDER + OUTPUT_SUBFOLDER_IMAGES):
        os.makedirs(OUTPUT_FOLDER + OUTPUT_SUBFOLDER_IMAGES)        
    shutil.copyfile(input_image_file, OUTPUT_FOLDER + OUTPUT_SUBFOLDER_IMAGES + new_filename) 
      
    return {'filename': new_filename, 'id': id, 'name': mushroom, 'tagged': tag_status}
      
def appendOneImageToHtml(output_html_file,image_file,mushId,mushroom,tagged, counter):
    item_html = """
    <figure class="houba"><img alt="Houba" src="<IMAGE_FILE>" style="width: 300px; height: 300px;">
        <figcaption><ID><br>
        <span><MUSHROOM></span></figcaption>
    </figure>
    """
    capt = mushroom + ' ({})'.format(mushId)
    item_html = item_html.replace('<IMAGE_FILE>',OUTPUT_SUBFOLDER_IMAGES + image_file)
    item_html = item_html.replace('<ID>',str(counter)+tagged)
    item_html = item_html.replace('<MUSHROOM>', capt)
    
    f = open(output_html_file, "r", encoding='cp1250')
    contents = f.readlines()
    f.close()

    contents.insert(52, item_html)
    
    f = open(output_html_file, "w", encoding='cp1250')
    contents = "".join(contents)
    f.write(contents)
    f.close()

def shroom_url_tag(path,urlMode):
    """
        Rozparsuje URL houby a vrati tag ID - funguje jen pro houby, ktere skutecne maji tag v nazvu!
        path - cesta k souboru/url k naparsovani
        urlMode - dequote url?
    """

    if urlMode:
        # Pokud slo o URL, bude treba dekodovat + odriznout vse za koncovkou souboru (alt= atd.)
        text = urllib.parse.unquote(path)
        if text.find('jpg?alt') != -1:
            text = text.split('?alt')[0]
    else:
        text = path

    # Získáme název souboru
    splitPath = text.split('/')
    filename = splitPath[len(splitPath)-1]

    # Splitneme dle tečky (kterou čekáme před koncovkou a tagem)
    splitFile = filename.split('.')

    # Tag ID cekame na predposledni pozici pole (ale radsi koukneme, jestli tam skutecne je 'Txxx')
    if 'T' in splitFile[len(splitFile)-2]:
        return int(splitFile[len(splitFile)-2].strip('T'))#-1 # v db je druh od 1, v tomdle skriptu od 0
    else:
        return None
    
def shroom_url_bestguess(path,urlMode,ignoreLocationless):
    """
    Rozparsuje URL houby na GPS souradnice a dict s polozkami species_idx:prob.
    path - cesta k souboru/url k naparsovani
    urlMode - dequote url?
    ignoreLocationless - ignorovat soubory bez informace o lokalite
    """
    res = {}

    if urlMode:
        # Pokud slo o URL, bude treba dekodovat + odriznout vse za koncovkou souboru (alt= atd.)
        text = urllib.parse.unquote(path)
        if text.find('jpg?alt') != -1:
            text = text.split('?alt')[0]
    else:
        text = path

    # Bohuzel, protoze jsme v aplikaci pouzili jako separator inforamci v nazvu souboru spojovnik/minus,
    # je treba osetrovat, kdy jedna a nebo obě koordináty jsou záporné (a rozparsováním názvu získáme
    # delší pole.

    # Získáme název souboru
    splitPath = text.split('/')
    filename = splitPath[len(splitPath)-1]

    # Získáme pole fakt + jeho délku
    splitFile = filename.split('-')
    ln = len(splitFile)
    probabilities = '' #pole na druhy a pravdepodobnosti

    if ln==5:
        # mohlo by jit o soubor bez lokality, funkce se zachova dle flagu
        if splitFile[2]=='NA':
            if ignoreLocationless:
                return ({})
            else:
                res['lat'] = None
                res['lon'] = None


        else:
        # nebo o 'standardni' pripad pro severni sirku a vychodni delku - sirka a delka je na pozici 2
            latlen = splitFile[2]
            splitLatlen = latlen.split(',')

            res['lat'] = float(splitLatlen[0])
            res['lon'] = float(splitLatlen[1])

        # kazdopadne psti jsou na pozici 4
        probabilities = splitFile[4]

    elif ln == 6:
        # jedna ze souradnic je zaporna

        # pokud je to sirka, bude pozice 2 prazdna "--lat" v nazvu
        if splitFile[2] == '':
            splitLatlen = splitFile[3].split(',')

            res['lat'] = float('-' + splitLatlen[0]) #musime doplnit minus
            res['lon'] = float(splitLatlen[1])
        # pokud nebyla zaporna sirka, byla zaporna delka
        else:
            res['lat'] = float(splitFile[2].strip(',')) #musime oriznout carku
            res['lon'] = float('-' + splitFile[3])      #musime doplnit minus

        # kazdopadne psti jsou na pozici 5
        probabilities = splitFile[5]

    elif ln == 7:
        # obe souradnice jsou zaporne, sirka je pak 3 a delka 4
        res['lat'] = float('-' + splitFile[3].strip(',')) #musime oriznout carku a doplnit minus
        res['lon'] = float('-' + splitFile[4])            #musime doplnit minus

        # kazdopadne psti jsou na pozici 6
        probabilities = splitFile[6]

    # Nyni jiz jen naparsujeme pravdepodobnosti

    species_text = probabilities
    prob_dict = {}

    #vohek
    speciesTextSplit = species_text.split('.')
    res = int(speciesTextSplit[0].split(':')[0])

    return(res)

# MAIN
# MAIN
# MAIN

#Clear output
if os.path.exists(OUTPUT_FOLDER):
    shutil.rmtree(OUTPUT_FOLDER)
os.mkdir(OUTPUT_FOLDER)

# Prepare steering vars
idToNameBridge = readBridge()
fileCounter = 400
output_file_index = 4

output_file = createHtmlSkeleton(output_file_index)

#Start with tagged images
for entry in os.scandir(INPUT_FOLDER_TAG):
    image_file_details = renameAndIncludeImage(True, idToNameBridge, entry, fileCounter)
    appendOneImageToHtml(output_file,image_file_details['filename'], image_file_details['id'],
                         image_file_details['name'],image_file_details['tagged'],fileCounter)    
    fileCounter+=1
    if fileCounter % 100 == 0:
        print(str(fileCounter))
        output_file_index+=1
        output_file = createHtmlSkeleton(output_file_index)
 
#Procced with untagged images
for entry in os.scandir(INPUT_FOLDER_NOTAG):
    image_file_details = renameAndIncludeImage(False, idToNameBridge, entry, fileCounter)
    appendOneImageToHtml(output_file,image_file_details['filename'], image_file_details['id'],
                         image_file_details['name'],image_file_details['tagged'],fileCounter)    
    fileCounter+=1
    if fileCounter % 100 == 0:
        output_file_index+=1
        output_file = createHtmlSkeleton(output_file_index)

